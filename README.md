# lidar3d_roi Package

## Introduction

The purpose of this package is to Model and subtract the background points from the pointcloud.
the bakground is the non-moving points/voxels.

## Getting started
To get and Build this repository:

```
$ mkdir -p lidar3d_ws/src
$ cd lidar3d_ws/src
$ git clone https://gricad-gitlab.univ-grenoble-alpes.fr/ama_lig_pcl_roi/pcl_bg_subtract.git
$ cd ../..
$ catkin init
$ catkin config -DCMAKE_BUILD_TYPE=RELEASE
$ catkin build
```

Then to run the code (*from lidar3d_ws*):
```
$ # Source your WS
$ . devel/setup.bash
$ # Change your_pointcloud_2_topic_name to your topic name
$ # RViz is enabled by default here, if you want to disable it use the flag: rviz:=false
$ roslaunch lidar3d_roi roi_extraction.launch input_cloud_topic:=your_pointcloud_2_topic_name
```
## LICENSE
LGPL V3.1