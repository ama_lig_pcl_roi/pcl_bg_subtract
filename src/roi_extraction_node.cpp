#include <cstdlib>
#include <stdexcept>

#include <ros/ros.h>

#include <lidar3d_roi/roi_extraction.hpp>

namespace
{

/**
 * \brief Name for ROS logging in the 'init' context.
 */
constexpr char ROS_LOG_INIT[]{"init"};

/**
 * \brief Name for ROS logging in the 'main' context.
 */
constexpr char ROS_LOG_MAIN[]{"main"};
}

using namespace lidar_tracking;

/*********
 * MAIN **
*********/
int main(int argc, char **argv)
{
    //--------------------------------------------------------
    // Preparations
    //--------------------------------------------------------
    ros::init(argc, argv, "lidar3d_roi");

    // Create a node handle, in the namespace where
    // the node should publish messages in.
    ros::NodeHandle nh_;

    ROS_WARN_STREAM_NAMED(ROS_LOG_INIT, "=== 3D LiDAR ROI Extraction Node is Ready ===");
    //--------------------------------------------------------
    // Start node execution
    //--------------------------------------------------------
    try
    {
        LidarROI lidar_roi_(nh_);
        ros::spin();
    }
    catch(const std::runtime_error& exception)
    {
        ROS_FATAL_STREAM_NAMED(ROS_LOG_MAIN, "Runtime error: '" << exception.what() << "'");
        return EXIT_FAILURE;
    }
    catch(const std::exception& exception)
    {
        ROS_FATAL_STREAM_NAMED(ROS_LOG_MAIN, "Exception '" << exception.what() << "'");
        return EXIT_FAILURE;
    }
    catch(...)
    {
        ROS_FATAL_NAMED(ROS_LOG_MAIN, "Unknown exception");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}