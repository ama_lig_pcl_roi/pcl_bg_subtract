#include <iostream>
#include <math.h>
#include <typeinfo>
#include <cstdlib>
#include <stdexcept>
// #include <vector>
#include <ctime>
#include <map>
// #include <unordered_map>
#include <unordered_set>

#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/impl/point_types.hpp>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/impl/voxel_grid.hpp>
#include <pcl/pcl_macros.h>
#include <pcl/io/pcd_io.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/impl/instantiate.hpp>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/impl/kdtree_flann.hpp>  
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/impl/extract_indices.hpp>
#include <pcl/PointIndices.h>
#include <pcl/impl/pcl_base.hpp>
#include <pcl/conversions.h>
#include <pcl/impl/point_types.hpp>
#include <pcl/features/integral_image_normal.h>
#include <pcl/features/impl/integral_image_normal.hpp>
#include <pcl/search/search.h>
#include <pcl/search/impl/search.hpp>
#include <pcl/search/kdtree.h>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/search/organized.h>
#include <pcl/search/impl/organized.hpp>
#include <pcl/features/normal_3d.h>
#include <pcl/features/impl/normal_3d.hpp>
#include <pcl/common/centroid.h>
#include <pcl/common/impl/centroid.hpp>
#include <pcl/common/transforms.h>
#include <pcl/range_image/range_image_planar.h>
#include <pcl/octree/octree.h>
#include <pcl/octree/octree_search.h>
// #include <pcl/octree/octree_pointcloud_voxelgrid.h>
// #include <pcl/octree/octree_pointcloud_adjacency.h>
// #include <pcl/octree/octree_pointcloud_voxelcentroid.h>
// #include <pcl/visualization/cloud_viewer.h>

// #include <visualization_msgs/Marker.h>
// #include <visualization_msgs/MarkerArray.h>
//#include <rviz_visual_tools/rviz_visual_tools.h>

#include "lidar3d_roi/roi_extraction.hpp"

namespace
{

/**
 * \brief Name for ROS logging in the 'init' context.
 */
constexpr char ROS_LOG_INIT[]{"init"};

}

using namespace lidar_tracking;

LidarROI::LidarROI(ros::NodeHandle &nh_) : 
    frame_id_(0),
    voxel_id_reference(0),
    ns(""),
    /**************************
     * TODO: remove this param
    ***************************/
    track_dropped(0), 
    tracking(false),
    cloud_reference(new OusterPCL),
    cloud_complete(new OusterPCL),
    cloud_roi(new OusterPCL)
{
    bool ns_exist = nh_.getParam("/namespace", ns);
    if(ns_exist){ns = "/"+ns;}else{ns = "";};
    ROS_WARN_STREAM("namespace  "<< ns);

    nh_.getParam(ns+"/input_topic_name", input_topic_name);
    // input_topic_name = ns+input_topic_name;
    ROS_WARN_STREAM("input_topic_name  "<< input_topic_name);
    
    nh_.getParam(ns+"/output_topic_name", output_topic_name);
    ROS_WARN_STREAM("output_topic_name  "<< output_topic_name);

    nh_.getParam(ns+"/max_voxel_size", max_voxel_size);
    max_voxel_size = (float)max_voxel_size;
    ROS_WARN_STREAM("max_voxel_size  "<< max_voxel_size);

    nh_.getParam(ns+"/min_voxel_density", min_voxel_density);
    ROS_WARN_STREAM("min_voxel_density  "<< min_voxel_density);

    nh_.getParam(ns+"/max_attempts", max_attempts);
    ROS_WARN_STREAM("max_attempts  "<< max_attempts);
    
    roi_pcl_pub = nh_.advertise<sensor_msgs::PointCloud2>(output_topic_name, 1);
    pt_cloud2_subscriber_= nh_.subscribe(input_topic_name, 1, &LidarROI::init, this);
}

LidarROI::~LidarROI(){}

void LidarROI::init(const sensor_msgs::PointCloud2ConstPtr &pt_cloud_2_msg)
{
    ROS_WARN_STREAM_NAMED(ROS_LOG_INIT, "frame start:" << frame_id_);

    // conversion: pointcloud2 --> pcl <OusterPoint>
    pcl::fromROSMsg(*pt_cloud_2_msg, *cloud_complete);

    // loop points and assign voxel id = -1
    initialize(cloud_complete);

    findVoxelList(cloud_complete, cloud_roi, &voxel_list);

    pcl::toROSMsg(*cloud_roi, pt_cloud2_roi); 

    pt_cloud2_roi.header = pt_cloud_2_msg->header;

    roi_pcl_pub.publish(pt_cloud2_roi);

    frame_id_++;

    ros::spin();
}

void LidarROI::createVoxels(OusterPCL::Ptr cloud, std::vector<Voxel> *voxel_list)
{
    pcl::KdTreeFLANN<OusterPoint> kdtree;
    kdtree.setInputCloud(cloud);
    std::vector<int> pt_idx_vec;
    std::vector<float> pt_dist_vec;
    int voxel_id = 0;
    for (auto &point : *cloud)
    {
        // if(point.range <= 2000){
        if (point.voxel_id == -1)
        {
            if (kdtree.radiusSearch(point, max_voxel_size, pt_idx_vec, pt_dist_vec) > min_voxel_density)
            {
                // create voxel if neighbours are found:
                Voxel voxel;
                voxel_id ++;
                point.voxel_id = voxel_id;
                voxel.voxel_id = voxel_id;
                // assign the search point to be the center of the voxel;
                voxel.center = point;
                voxel.member_points.push_back(point);
                for (std::size_t i = 0; i < pt_idx_vec.size(); ++i)
                {
                    (*cloud)[pt_idx_vec[i]].voxel_id = voxel_id;
                    voxel.member_points.push_back((*cloud)[pt_idx_vec[i]]);
                }
                (*voxel_list).push_back(voxel);
            }
        }
        // }
    }
}

void LidarROI::createVoxelsMovement(OusterPCL::Ptr cloud)
{
    pcl::KdTreeFLANN<OusterPoint> kdtree;
    std::vector<int> pt_idx_vec;
    std::vector<float> pt_dist_vec;

    kdtree.setInputCloud(cloud);
    if (reference_voxel_list.size() == 0)
    {
        for (auto &point : *cloud)
        {
            if (point.voxel_id == -1)
            {
                if (kdtree.radiusSearch(point, max_voxel_size, pt_idx_vec, pt_dist_vec) > min_voxel_density)
                {
                    // create voxel if neighbours are found:
                    Voxel voxel;
                    voxel_id_reference += 1;
                    point.voxel_id = voxel_id_reference;
                    voxel.voxel_id = voxel_id_reference;
                    // assign the search point to be the center of the voxel;
                    voxel.center = point;
                    voxel.member_points.push_back(point);
                    for (std::size_t i = 0; i < pt_idx_vec.size(); ++i)
                    {
                        (*cloud)[pt_idx_vec[i]].voxel_id = voxel_id_reference;
                        voxel.member_points.push_back((*cloud)[pt_idx_vec[i]]);
                    }
                    (reference_voxel_list).push_back(voxel);
                }
            }
        }
    }
    else
    {
        for (auto voxel : reference_voxel_list)
        {
            if (kdtree.radiusSearch(voxel.center, max_voxel_size, pt_idx_vec, pt_dist_vec) > min_voxel_density)
            {
                for (std::size_t i = 0; i < pt_idx_vec.size(); ++i)
                {
                    (*cloud)[pt_idx_vec[i]].voxel_id = voxel.voxel_id;
                    voxel.member_points.push_back((*cloud)[pt_idx_vec[i]]);
                }
            }
        }
    }
}

void LidarROI::findNewVoxels(OusterPCL::Ptr cloud, std::vector<Voxel> *new_voxel_list, bool update)
{
    pcl::KdTreeFLANN<OusterPoint> kdtree;
    std::vector<int> pt_idx_vec;
    std::vector<float> pt_dist_vec;

    kdtree.setInputCloud(cloud);
    for (auto &point : *cloud)
    {
        if (point.voxel_id == -1)
        {
            if (kdtree.radiusSearch(point, max_voxel_size, pt_idx_vec, pt_dist_vec) > min_voxel_density)
            {
                std::vector<int> neighbor_index;
                for (std::size_t i = 0; i < pt_idx_vec.size(); ++i)
                {
                    OusterPoint neighbor = (*cloud)[pt_idx_vec[i]];
                    if (neighbor.voxel_id == -1)
                    {
                        neighbor_index.push_back(pt_idx_vec[i]);
                    }
                }
                if ((signed int)neighbor_index.size() > min_voxel_density)
                {
                    Voxel voxel;
                    voxel_id_reference += 1;
                    voxel.voxel_id = voxel_id_reference;
                    point.voxel_id = voxel_id_reference;
                    // assign the search point to be the center of the voxel;
                    voxel.center = point;
                    voxel.member_points.push_back(point);
                    for (auto idx : neighbor_index)
                    {
                        (*cloud)[idx].voxel_id = voxel_id_reference;
                        voxel.member_points.push_back((*cloud)[idx]);
                    }
                    if (update)
                    {
                        reference_voxel_list.push_back(voxel);
                    }
                    else
                    {
                        (*new_voxel_list).push_back(voxel);
                    }
                }
            }
        }
    }
}

void LidarROI::getVoxelCloud(OusterPCL::Ptr cloud_reference)
{
    for (auto voxel : reference_voxel_list)
    {
        cloud_reference->push_back(voxel.center);
    }
}

void LidarROI::getVoxelCloudROI(OusterPCL::Ptr cloud_roi, std::vector<Voxel> new_voxel_list)
{
    for (auto voxel : new_voxel_list)
    {
        for (auto point : voxel.member_points)
        {
            cloud_roi->push_back(point);
        }
    }
}

void LidarROI::movementDetection(
    OusterPCL::Ptr cloudComplete,
      OusterPCL::Ptr cloudReference,
      std::vector<Voxel> *new_voxel_list)
{
    createVoxelsMovement(cloudComplete);
    bool update = false;
    if ((frame_id_ < 10) & (frame_id_ > 5))
    { // should be f>3
        // calibration of reference
        update = true;
        findNewVoxels(cloudComplete, new_voxel_list, update);
        getVoxelCloud(cloudReference);
    }
    else
    {
        if ((frame_id_ > 5))
        { // f>3
            findNewVoxels(cloudComplete, new_voxel_list, update);
            getVoxelCloudROI(cloudReference, *new_voxel_list);
        }
    }
    ROS_WARN_STREAM(" voxel count " << new_voxel_list->size());
}

void LidarROI::roiCloud(OusterPCL::Ptr cloudIn, OusterPCL::Ptr cloudOut)
{
    float baseDiff = 0.025f;
    float dx = (track_map[0].attempt + 1) * baseDiff;
    float dy = (track_map[0].attempt + 1) * baseDiff;
    float max_length = 1.2f;
    float max_width = 1.2f;
    Eigen::Vector4d predicted_centroid;
    int lastKey = track_map[0].object_seq.rbegin()->first;
    int lastKeyCentroid = track_map[0].tracked_centroid_seq.rbegin()->first;
    if ((track_map[0].shift_seq[lastKey](0) == -99))
    {
        predicted_centroid = track_map[0].tracked_centroid_seq[lastKeyCentroid];
        dx += baseDiff;
        dy += baseDiff;
    }
    else
    {
    Eigen::Vector4d predicted_centroid;
    //int lastKey = track_map[0].object_seq.rbegin()->first;
    int lastKeyCentroid = track_map[0].tracked_centroid_seq.rbegin()->first;
    predicted_centroid(0) = track_map[0].tracked_centroid_seq[lastKeyCentroid](0) + track_map[0].shift_seq[lastKeyCentroid](0);
    predicted_centroid(1) = track_map[0].tracked_centroid_seq[lastKeyCentroid](1) + track_map[0].shift_seq[lastKeyCentroid](1);
    predicted_centroid(2) = track_map[0].tracked_centroid_seq[lastKeyCentroid](2);
    predicted_centroid(3) = track_map[0].tracked_centroid_seq[lastKeyCentroid](3);
    predicted_centroid = predicted_centroid;
    }
    pcl::PointCloud<OusterPoint> cloudOut_n;
    for (auto &point : *cloudIn)
    {
        if ((point.x >= predicted_centroid(0) - dx - max_length) & (point.x <= predicted_centroid(0) + dx + max_length))
        {
            if ((point.y >= predicted_centroid(1) - dy - max_width) & (point.y <= predicted_centroid(1) + dy + max_width))
            {
                cloudOut_n.push_back(point);
            }
        }
    }
    *cloudOut = cloudOut_n;
}

void LidarROI::findVoxelList(
    OusterPCL::Ptr cloudComplete,
      OusterPCL::Ptr cloudROI,
      std::vector<Voxel> *voxel_list)
{
    if (track_map.size() == 0)
    {
        if (frame_id_<= 5)
        { // should be 1
            ROS_WARN_STREAM(" ########### detection by classification");
            cloudROI = cloudComplete;
            createVoxels(cloudROI, voxel_list);
            tracking = false;
        }
        else
        {
            ROS_WARN_STREAM(" ########### movement detection");
            movementDetection(cloudComplete, cloudROI, voxel_list);
            tracking = false;
        }
    }
    else
    {
        if (track_map[0].attempt < max_attempts)
        {
            ROS_WARN_STREAM("tracking attempt " << track_map[0].attempt);
            roiCloud(cloudComplete, cloudROI);
            createVoxels(cloudROI, voxel_list);
            tracking = true;
        }
        else
        {
            ROS_WARN_STREAM("end tracking");
            track_map[0].attempt = 0;
            track_map.erase(0); // change when multiple
            track_dropped += 1;
            movementDetection(cloudComplete, cloudROI, voxel_list);
            tracking = false;
        }
    }
}
