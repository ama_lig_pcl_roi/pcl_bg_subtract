#ifndef UTIL_DS_H
#define UTIL_DS_H

#include <iostream>
#include <unordered_map>
#include <vector>

#include <ros/ros.h>

#include <pcl/impl/point_types.hpp>
#include <pcl/point_types.h>
#include <pcl/common/projection_matrix.h>

typedef unsigned int uint;
typedef int frame_id;

struct OusterPoint
{
    PCL_ADD_POINT4D;
    float intensity;
    uint t;
    unsigned short reflectivity;
    unsigned char ring;
    unsigned short ambient;
    uint range;
    int voxel_id;
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
} EIGEN_ALIGN16;

POINT_CLOUD_REGISTER_POINT_STRUCT(
    OusterPoint,
    (float, x, x)
    (float, y, y)
    (float, z, z)
    (float, intensity, intensity)
    (uint, t, t)
    (unsigned short, reflectivity, reflectivity)
    (unsigned char, ring, ring)
    (unsigned short, ambient, ambient)
    (uint, range, range)
)

typedef pcl::PointCloud<OusterPoint> OusterPCL;
typedef pcl::PointCloud<pcl::Normal> NormaPCL;

struct Voxel
{
    // id
    int voxel_id;
    // center:s
    OusterPoint center;
    // dimensions:
    float length; // from x
    float width;  // from y
    float height; // from z
    OusterPCL member_points;
    // std::vector<int> member_point_indices;
};

struct SuperVoxel
{
    Voxel voxel;
    float mean_intensity;
    float var_intensity;
    float mean_reflectivity;
    float var_reflectivity;
    float mean_ambient;
    float var_ambient;
    float mean_range;
    int object_id = -1;
    NormaPCL normals;
    Eigen::Vector4f normal;
};

struct Chain
{
    SuperVoxel principal_link;
    std::unordered_map<int, SuperVoxel> secondary_links;
    bool linked = false;
};

struct Object
{
    std::unordered_map<int, SuperVoxel> links;
    bool checked = false;
    bool joined = false;
    float mean_reflectivity;
    float mean_variance;
    float mean_intensity;
    float mean_ambient;
    float width;
    float length;
    float height;
    OusterPCL cloud;
    Eigen::Vector4d centroid;
    float distance;
    float person_votes = 0.0f;
    float background_votes = 0.0f;
    float nx_ratio;
    float ny_ratio;
    float nz_ratio;
    float mean_range;
};

struct Track
{
    int id;
    std::map<frame_id, Eigen::Vector2d> shift_seq;
    std::map<frame_id, Object> object_seq;
    std::map<frame_id, Eigen::Vector4d> tracked_centroid_seq;
    // this three maps store only the last 30 (3s) of data
    std::map<frame_id, bool> recovered_by_distance_seq;
    int attempt = 0;
};

#endif