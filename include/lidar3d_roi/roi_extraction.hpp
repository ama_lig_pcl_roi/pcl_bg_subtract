#ifndef ROI_EXT_H
#define ROI_EXT_H

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>

#include "lidar3d_roi/utility_functions.h"

namespace lidar_tracking
{

class LidarROI{
    private:

        /**
         * \brief Frame ID.
         */
        frame_id frame_id_;

        int voxel_id_reference;

        bool tracking;

        float max_voxel_size=0.2f;

        int min_voxel_density;

        int max_attempts;

        int track_dropped;

        std::map<int, Track> track_map;

        /**
         * \brief Publisher for general system states.
         */
        ros::Publisher roi_pcl_pub;

        /**
         * \brief Subscriber of input pointcloud2.
         */
        ros::Subscriber pt_cloud2_subscriber_;

        std::string ns;

        /**
         * \brief Subscriber input topic name.
         */
        std::string input_topic_name;

        /**
         * \brief Publisher output topic name.
         * default name "lidar3d_roi".
         */
        std::string output_topic_name;

        std::vector<Voxel> voxel_list;

        std::vector<Voxel> reference_voxel_list;

        OusterPCL::Ptr cloud_reference;

        OusterPCL::Ptr cloud_complete;

        OusterPCL::Ptr cloud_roi;

        sensor_msgs::PointCloud2 pt_cloud2_roi;
        
    public:

		/**
		 * \brief Constructor.
		 */
		LidarROI(ros::NodeHandle&);

		/**
		 * \brief Destructor.
		 */
		virtual ~LidarROI();

        void createVoxels(OusterPCL::Ptr , std::vector<Voxel>*);

        void createVoxelsMovement(OusterPCL::Ptr );

        void findNewVoxels(OusterPCL::Ptr, std::vector<Voxel>*, bool);

        void getVoxelCloud(OusterPCL::Ptr);

        void getVoxelCloudROI(OusterPCL::Ptr , std::vector<Voxel> );

        void movementDetection(OusterPCL::Ptr, OusterPCL::Ptr, std::vector<Voxel>*);

        void roiCloud(OusterPCL::Ptr, OusterPCL::Ptr );

        void findVoxelList( OusterPCL::Ptr, OusterPCL::Ptr, std::vector<Voxel>*);

        void init(const sensor_msgs::PointCloud2ConstPtr&);
};


}


#endif