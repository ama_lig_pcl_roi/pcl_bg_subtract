#ifndef UTIL_FUNCS_H
#define UTIL_FUNCS_H

#include <pcl/features/normal_3d.h>
#include <pcl/features/impl/normal_3d.hpp>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/centroid.h>
#include <pcl/common/impl/centroid.hpp>
#include <pcl/common/transforms.h>

#include "lidar3d_roi/utility_data_structures.h"

namespace lidar_tracking
{
    inline bool operator==(const SuperVoxel &lhs, const SuperVoxel &rhs)
    {
        return lhs.voxel.voxel_id == rhs.voxel.voxel_id;
    }

    inline bool operator==(const Eigen::Vector4d &lhs, const Eigen::Vector4d &rhs)
    {
        return lhs(0) == rhs(0) &&
               lhs(1) == rhs(1) &&
               lhs(2) == rhs(2);
    }

    inline void initialize(OusterPCL::Ptr cloud)
    {
        /***********************
         * Initialize Voxel ID *
        ************************/
        for (auto &point : *cloud)
        {
            point.voxel_id = -1;
        }
        /**************************************
         * Extract 32 rings if there are more *
         * https://github.com/ouster-lidar/ouster_example/issues/51
        ***************************************/
        if (cloud->height > 32)
        {
            ROS_WARN_STREAM("===== CLOUD Height > 32 !!!==== " << cloud->height);
            pcl::PointIndices::Ptr inliers(new pcl::PointIndices());
            pcl::ExtractIndices<OusterPoint> extract;
            for (unsigned int i = 0; i < (*cloud).size(); i++)
            {
                if ((static_cast<int>(cloud->points[i].ring) % 2 != 0))
                {
                    inliers->indices.push_back(i);
                }
            }
            extract.setInputCloud(cloud);
            extract.setIndices(inliers);
            extract.setNegative(true);
            extract.filter(*cloud);
        }
        /**************************************************
         *  demean (centralize around the mean) the cloud * 
         **************************************************/ 
        Eigen::Vector4d centroid;
        Eigen::Affine3d transform = Eigen::Affine3d::Identity();
        
        compute3DCentroid(*cloud, centroid);
        ROS_WARN_STREAM("------- centroid before -------" << centroid);

        transform.translation() << -centroid(0), -centroid(1), -centroid(2);
        pcl::transformPointCloud(*cloud, *cloud, transform);

        compute3DCentroid(*cloud, centroid); 
        ROS_WARN_STREAM("------- centroid after -------" << centroid);
    }
}

#endif
